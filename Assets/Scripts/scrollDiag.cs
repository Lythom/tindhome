﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrollDiag : MonoBehaviour {

    RectTransform t;
    // Start is called before the first frame update
    void Start () {
        t = (RectTransform) this.transform;
    }

    // Update is called once per frame
    void Update () {
        t.anchorMin -= new Vector2 (0.01f, 0.01f) * Time.deltaTime;
    }
}