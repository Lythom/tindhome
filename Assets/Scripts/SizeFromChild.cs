﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SizeFromChild : MonoBehaviour {
    public RectTransform child;
    public RectTransform t;
    public Vector2 padding;

    // Start is called before the first frame update
    void Start () {
        child = (RectTransform) this.transform.GetChild (0);
        t = (RectTransform) this.transform;
    }

    // Update is called once per frame
    void Update () {
        if (child != null) {
            t.sizeDelta = child.sizeDelta + padding;
        }
    }
}