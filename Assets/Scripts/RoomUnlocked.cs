﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class RoomUnlocked : MonoBehaviour {
    public RoomType room;
    public bool isUnlocked = false;
    public RoomLevel level;

    public Gauge gauge;
    public Text text;
    public Image image;

    // Start is called before the first frame update
    void Start () {
        gauge = GetComponentInChildren<Gauge> ();
        text = GetComponentInChildren<Text> ();
        image = GetComponent<Image> ();
    }

    // Update is called once per frame
    void Update () {
        if (text != null) {
            text.text = StringExtensions.FirstCharToUpper (Data.getRoomTypeLabel (room));
        }
        if (gauge != null) {
            gauge.value = isUnlocked ? (int) level + 1 : 0;
        }
        if (image != null) {
            // image.color;
            float targetAlpha = isUnlocked ? 1 : 0.15f;
            Color c = image.color;
            if (c.a != targetAlpha) {
                image.color = new Color (c.r, c.g, c.b, targetAlpha - (targetAlpha - c.a) * 0.9f);
                text.color = new Color (text.color.r, text.color.g, text.color.b, image.color.a);
            }
        }
    }
}