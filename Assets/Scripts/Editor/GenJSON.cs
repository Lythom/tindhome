using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class GenJSON : MonoBehaviour {
    [MenuItem ("(¯`·._.·[ TindHome ]·._.·´¯)/Generate Json exemples and doc")]
    public static void GenerateJSON () {

        // HOUSES
        var houses = new House[2];
        houses[0] = new House ();
        houses[0].name = "name";
        houses[0].description = "description";
        houses[0].condition = Condition.good;
        houses[0].archetype = Archetype.MobileHome;
        houses[0].rooms = new Room[2];
        houses[0].rooms[0] = new Room ();
        houses[0].rooms[0].type = RoomType.bedroom;
        houses[0].rooms[0].level = RoomLevel.cheap;
        houses[0].rooms[1] = new Room ();
        houses[0].rooms[1].type = RoomType.bedroom;
        houses[0].rooms[1].level = RoomLevel.cocooning;
        QuestionDeck deck = new QuestionDeck ();
        deck.info = new QuestionReponse ();
        deck.info.question = "Question pour demander la valeur d'une info ?";
        deck.info.response = "Réponse qui permet de connaitre la valeur de l'info";
        deck.challenge = new QuestionChallenge ();
        deck.challenge.question = "Question pour challenger sur une info ?";
        deck.challenge.rightResponse = "Bonne réponse !";
        deck.challenge.wrongResponses = new string[] { "Mauvaise réponse 1", "Mauvaise réponse 2" };
        houses[0].decks = new QuestionDeck[2];
        houses[0].decks[0] = deck;
        houses[0].decks[1] = deck;
        houses[1] = houses[0];

        string json = JsonHelper.arrayToJson<House> (houses);
        File.WriteAllText (Application.streamingAssetsPath + "/houses.exemple.json", json);
        Debug.Log ("Updated " + Application.streamingAssetsPath + "/houses.exemple.json");

        // CONFIG
        Config config = new Config ();
        int roomTypesCount = System.Enum.GetValues (typeof (RoomType)).Length;
        int conditionsCount = System.Enum.GetValues (typeof (Condition)).Length;
        int archetypesCount = System.Enum.GetValues (typeof (Archetype)).Length;

        config.roomTypeLevelPoints = new ConditionPoints[roomTypesCount];
        for (int i = 0; i < roomTypesCount; i++) {
            config.roomTypeLevelPoints[i] = new ConditionPoints ();
            config.roomTypeLevelPoints[i].points = new int[conditionsCount];
            for (int j = 0; j < conditionsCount; j++) {
                config.roomTypeLevelPoints[i].points[j] = i + i * j;
            }
        }
        config.archetypeMultiplicator = new int[archetypesCount];
        for (int i = 0; i < archetypesCount; i++) {
            config.archetypeMultiplicator[i] = 10 + i * i * 5;
        }
        config.conditionMultiplicator = new float[conditionsCount];
        for (int i = 0; i < conditionsCount; i++) {
            config.conditionMultiplicator[i] = 0.9f + 0.1f * i;
        }
        File.WriteAllText (
            Application.streamingAssetsPath + "/config.exemple.json",
            JsonUtility.ToJson (config, true)
        );
        Debug.Log ("Updated " + Application.streamingAssetsPath + "/config.exemple.json");

        // DECKS
        // QuestionDeck deck = new QuestionDeck ();
        // deck.infos = new QuestionReponse[2];
        // deck.infos[0] = new QuestionReponse ();
        // deck.infos[0].question = "Question pour demander la valeur d'une info ?";
        // deck.infos[0].response = "Réponse qui permet de connaitre la valeur de l'info";
        // deck.infos[1] = new QuestionReponse ();
        // deck.infos[1].question = "Autre question pour demander la valeur d'une info ?";
        // deck.infos[1].response = "Autre réponse qui permet de connaitre la valeur de l'info";
        // deck.challenges = new QuestionChallenge[2];
        // deck.challenges[0] = new QuestionChallenge ();
        // deck.challenges[0].question = "Question pour challenger sur une info ?";
        // deck.challenges[0].rightResponse = "Bonne réponse !";
        // deck.challenges[0].wrongResponses = new string[] { "Mauvaise réponse 1", "Mauvaise réponse 2" };
        // deck.challenges[1] = new QuestionChallenge ();
        // deck.challenges[1].question = "Autre question pour challenger sur une info ?";
        // deck.challenges[1].rightResponse = "Autre Bonne réponse !";
        // deck.challenges[1].wrongResponses = new string[] { "Autre Mauvaise réponse 1", "Autre Mauvaise réponse 2" };

        // List<string> decksPath = new List<string> ();
        // for (int i = 0; i <= 2; i++) {
        //     decksPath.Add ("houseLevel-" + i + ".json");
        // }
        // foreach (RoomType value in System.Enum.GetValues (typeof (RoomType))) {
        //     for (int i = 0; i <= 2; i++) {
        //         decksPath.Add (value.ToString () + "Count-" + i + ".json");
        //         decksPath.Add (value.ToString () + "Condition-" + i + ".json");
        //     }
        // }
        // var count = 0;
        // foreach (var deckPath in decksPath) {
        //     var fullPath = Application.streamingAssetsPath + "/decks/" + deckPath;
        //     if (!File.Exists (fullPath)) {
        //         File.WriteAllText (fullPath, JsonUtility.ToJson (deck, true));
        //         count++;
        //     }
        // }
        // Debug.Log ("Updated " + count + " missing deck in " + Application.streamingAssetsPath + "/decks");

        // README
        string references = "# Les données de TindHome\n\n";
        references += "Les valeurs de ce document ne peuvent être modifiées que dans le code. Demander à un Dave si besoin.\n";
        references += "\n## Emplacement des fichiers de données\n\n";
        references += "La liste des maison est éditable dans `StreamingAssets/houses.json`\n\n";
        // references += "Les listes de questions sont éditables dans `StreamingAssets/decks/<info>-<value>.json`\n";
        // references += "où un deck correspond à la liste des question/réponses d'info ET la liste des questions/mauvaisesReponses/bonneReponse du challenge\n";
        // references += "où <info> est une des infos possibles à obtenir et <value> est la valeur à obtenir/deviner (voir listes ci dessous)\n";

        // references += System.String.Join ("\n", decksPath.Select (p => "- " + p).ToArray ());

        references += "\n\n\n## Signification des valeurs\n";
        references += "\n### House level\n";
        int idx = 0;
        for (int i = 0; i < 3; i++) {
            references += idx + " = " + Data.getHouseLevelLabel (i).ToString () + "\n";
            idx++;
        }
        references += "\n### House archetype\n";
        idx = 0;
        foreach (Archetype value in System.Enum.GetValues (typeof (Archetype))) {
            references += idx + " = " + Data.getArchetypeLabel (value) + "\n";
            idx++;
        }

        references += "\n### Room conditions\n";
        idx = 0;
        foreach (Condition value in System.Enum.GetValues (typeof (Condition))) {
            references += idx + " = " + Data.getConditionLabel (value) + "\n";
            idx++;
        }

        references += "\n### Room types\n";
        idx = 0;
        foreach (RoomType value in System.Enum.GetValues (typeof (RoomType))) {
            references += idx + " = " + Data.getRoomTypeLabel (value) + "\n";
            idx++;
        }

        references += "\n### Room level\n";
        idx = 0;
        for (int i = 0; i < 3; i++) {
            references += idx + " = " + Data.getRoomLevelLabel ((RoomLevel)i).ToString () + "\n";
            idx++;
        }

        File.WriteAllText (Application.dataPath + "/README.md", references);
        Debug.Log ("Updated " + Application.dataPath + "/README.md");
    }
}