﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class Gauge : MonoBehaviour {
    public int value = 0;
    public int maxValue = 3;
    public RectTransform bg;
    public RectTransform fg;

    public int displayedValue = 0;

    // Start is called before the first frame update
    void Start () {
        bg = GetComponent<RectTransform> ();
        fg = transform.GetChild (0).GetComponent<RectTransform> ();
        displayedValue = value;
    }

    // Update is called once per frame
    void Update () {
        bg.sizeDelta = new Vector2 (60, 60 * maxValue);
        if (displayedValue != value) {
            AnimateNewValue (displayedValue, value);
            displayedValue = value;
        }
    }

    private void AnimateNewValue (float prev, float next) {
        StartCoroutine (DoAnimateNewValue (1f, prev, next));
    }

    IEnumerator DoAnimateNewValue (float durationInSeconds, float prev, float next) {
        for (float i = 0; i < durationInSeconds; i += Time.deltaTime) {
            float transition = CustumEaseEnd1 (i / durationInSeconds) * (next - prev);
            fg.sizeDelta = new Vector2 (60, 60 * (prev + transition));
            yield return null; // wait next frame before continuing
        }
        fg.sizeDelta = new Vector2 (60, 60 * next);
    }

    public void Shake (float durationInSeconds) {
        StartCoroutine (DoShake (durationInSeconds));
    }

    IEnumerator DoShake (float durationInSeconds) {
        Quaternion originalRotation = this.transform.localRotation;
        for (float i = 0; i < durationInSeconds; i += Time.deltaTime) {
            float angle = CustumEase (i / durationInSeconds);
            this.transform.localRotation = Quaternion.Euler (0, 0, (angle - 0.5f) * 25);
            yield return null; // wait next frame before continuing
        }
        this.transform.localRotation = originalRotation;
    }

    // https://github.com/shohei909/tweencore-sharp + http://tweenx.spheresofa.net/core/custom/#%7B%22time%22%3A0.75%2C%22easing%22%3A%5B%22Op%22%2C%5B%22Op%22%2C%5B%22Simple%22%2C%5B%22Standard%22%2C%22Quad%22%2C%22OutIn%22%5D%5D%2C%5B%22Lerp%22%2C1.1%2C0.64%5D%5D%2C%5B%22Op%22%2C%5B%22Op%22%2C%5B%22Op%22%2C%5B%22Simple%22%2C%5B%22Standard%22%2C%22Quad%22%2C%22Out%22%5D%5D%2C%5B%22RoundTrip%22%2C%22Yoyo%22%5D%5D%2C%5B%22Repeat%22%2C2%5D%5D%2C%22Multiply%22%5D%5D%7D
    public static float CustumEase (float rate) {
        return TweenCore.FloatTools.Lerp (TweenCore.Easing.QuadOutIn (rate), 1.1f, 0.64f) * TweenCore.FloatTools.Yoyo (TweenCore.FloatTools.Repeat (TweenCore.FloatTools.Lerp (rate, 0, 2f), 0, 1), TweenCore.Easing.QuadOut);
    }

    // http://tweenx.spheresofa.net/core/custom/#%7B%22time%22%3A0.75%2C%22easing%22%3A%5B%22Op%22%2C%5B%22Op%22%2C%5B%22Simple%22%2C%5B%22Standard%22%2C%22Warp%22%2C%22InOut%22%5D%5D%2C%5B%22RoundTrip%22%2C%22Yoyo%22%5D%5D%2C%5B%22Repeat%22%2C2.5%5D%5D%7D
    public static float CustumEaseEnd1 (float rate) {
        return TweenCore.FloatTools.Yoyo(TweenCore.FloatTools.Repeat(TweenCore.FloatTools.Lerp(rate, 0, 2.5f), 0, 1), TweenCore.Easing.WarpInOut);
    }
}