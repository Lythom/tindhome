﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelPageMaison : MonoBehaviour
{
    // it's the panel who will be display
    public GameObject OpenPanel;
    // it's the panel who will be hidde
    public GameObject ClosePanel;

    public void ChangePannel()
    {
        if(OpenPanel != null)
        {
            ClosePanel.SetActive(false);
            OpenPanel.SetActive(true);
        }
    }


}
