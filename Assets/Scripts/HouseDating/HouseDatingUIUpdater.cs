﻿using UnityEngine;
using UnityEngine.UI;

public class HouseDatingUIUpdater : MonoBehaviour {

    public Gauge gauge;
    public Text[] houseNameTexts;
    public Text houseDescription;

    void Start () {
        House h = Data.GetHouse (GameState.current.targetHouseId);
        if (h == null) return;
        foreach (var houseNameText in houseNameTexts) {
            houseNameText.text = h.name;
        }
        string str = "Type : " + Data.getArchetypeLabel (h.archetype) + "\n" +
            "État : " + Data.getConditionLabel (h.condition) + "\n" +
            "Loyer : " + h.getRent ()+ " €\n";
        houseDescription.text = str;
    }

    // Update is called once per frame
    void Update () {
        var p = GameState.current.getTargetProgress ();
        if (p != null) {
            gauge.value = p.patience;
            gauge.maxValue = Data.config.maxJauge;
        }
    }
}