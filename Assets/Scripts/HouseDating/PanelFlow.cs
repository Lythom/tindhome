﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PanelFlow : MonoBehaviour {

    public GameObject PanelPageMaison;
    public Text PanelPageMaisonText;
    public GameObject PanelInfo;
    public GameObject PanelPageChoper;
    public GameObject PanelPageUpgrade;
    public Button Info;
    public Button Choper;
    public Button back;
    public Button[] backToHouseSelection;
    public Button LeftAnswerChoperButtons;
    public Button CenterAnswerChoperButtons;
    public Button RightAnswerChoperButtons;
    public Button[] infoAnswerButtons;
    public GameObject PrefabButtonInfo;
    public Text ChallengeQuestionText;
    public Text ChanceText;
    public ModelDisplay ModelDisplayScript;
    public Gauge gauge;

    public static int index;

    public FeedbackBubble FeedbackPrefab;

    // Start is called before the first frame update
    void Start () {
        Info.onClick.AddListener (TriggerInfoFlow);
        Choper.onClick.AddListener (() => {
            index = 0;
            GameState.current.getTargetProgress ().approachPoints = 0;
            TriggerChoperFlow ();
        });

        back.onClick.AddListener (() => {
            PanelPageMaison.SetActive (true);
            PanelInfo.SetActive (false);
        });

        foreach (Button b in backToHouseSelection) {
            b.onClick.AddListener (GotoHouseSelection);
        }
    }

    void GotoHouseSelection () {
        SceneManager.LoadScene ("HouseSelection");
    }

    void TriggerInfoFlow () {
        Progress progress = GameState.current.getTargetProgress ();
        if (progress.patience == 0) {
            var f = Instantiate (FeedbackPrefab, Info.transform);
            f.text = "Je fatigue, reviens plus tard.";
            f.width = 250;
            gauge.Shake(0.3f);
            return;
        }
        House target = Data.GetHouse (GameState.current.targetHouseId);
        if (target == null) throw new Exception ("Error, house must not be null");
        PanelPageMaison.SetActive (false);
        PanelInfo.SetActive (true);
        QuestionDeck[] decks = target.decks;
        // Pour chaque bouton on chercher à faire correspondre un deck
        // si pas de deck on cache le bouton
        for (int i = 0; i < infoAnswerButtons.Length; i++) {
            Button b = infoAnswerButtons[i];
            if (i < decks.Length) {
                QuestionDeck deck = decks[i];
                Text t = b.GetComponentInChildren<Text> ();
                t.text = deck.info.question;

                // pose la question
                b.onClick.RemoveAllListeners ();
                b.onClick.AddListener (() => {
                    PanelInfo.SetActive (false);
                    PanelPageMaison.SetActive (true);
                    PanelPageMaisonText.text = deck.info.response;

                    // update progress
                    progress.patience -= Data.config.patienceQuestionCost;
                    // already asked ?
                    if (progress.alreadyAsked.Contains (deck.info.question)) {
                        progress.patience -= Data.config.patienceCostRepeat;
                        PanelPageMaisonText.text += "\nTu me l'as déjà demandé !";
                    } else {
                        progress.alreadyAsked.Add (deck.info.question);
                    }
                    GameState.current.PassTurn (progress);
                });
                b.gameObject.SetActive (true);
            } else {
                b.gameObject.SetActive (false);
            }

        }

    }

    void handleAnswerClick (bool isCorrectAnswer, int deckSize, Button b) {
        //if good answer
        if (isCorrectAnswer) {
            GameState.current.getTargetProgress ().approachPoints += Data.config.rightAnswerApproachBonus;
            PlayRightAnswerFeedback (b);
        } else {
            PlayWrongAnswerFeedback (b);
        }
        //if all cases we go to next question
        index++;
        if (index < deckSize) {
            TriggerChoperFlow ();
        } else {
            PanelPageChoper.SetActive (false);
            float chances = getChancesWith (Data.GetHouse (GameState.current.targetHouseId));
            TryChoper (chances);
        }
    }

    void TriggerChoperFlow () {
        Progress progress = GameState.current.getTargetProgress ();
        if (progress.patience == 0) {
            var f = Instantiate (FeedbackPrefab, Choper.transform);
            f.text = "Je fatigue, reviens plus tard.";
            f.width = 250;
            gauge.Shake(0.3f);
            return;
        }

        House target = Data.GetHouse (GameState.current.targetHouseId);
        if (target == null) throw new Exception ("Error, house must not be null");

        PanelPageMaison.SetActive (false);
        PanelPageChoper.SetActive (true);
        QuestionDeck[] decks = target.decks;
        // Pour chaque bouton on chercher à faire correspondre un deck
        // si pas de deck on cache le bouton
        QuestionDeck deck = decks[index];

        ChallengeQuestionText.text = deck.challenge.question;
        string[] question = new string[3];
        var reponse = deck.challenge.rightResponse;
        question[0] = deck.challenge.rightResponse;
        question[1] = deck.challenge.wrongResponses[0];
        question[2] = deck.challenge.wrongResponses[1];
        RandomHelper.Shuffle (question);
        Button b1 = LeftAnswerChoperButtons;
        Text t1 = b1.GetComponentInChildren<Text> ();
        t1.text = question[0];
        b1.onClick.RemoveAllListeners ();
        b1.onClick.AddListener (() => handleAnswerClick (t1.text == reponse, decks.Length, b1));

        Button b2 = CenterAnswerChoperButtons;
        Text t2 = b2.GetComponentInChildren<Text> ();
        t2.text = question[1];
        b2.onClick.RemoveAllListeners ();
        b2.onClick.AddListener (() => handleAnswerClick (t2.text == reponse, decks.Length, b2));

        Button b3 = RightAnswerChoperButtons;
        Text t3 = b3.GetComponentInChildren<Text> ();
        t3.text = question[2];
        b3.onClick.RemoveAllListeners ();
        b3.onClick.AddListener (() => handleAnswerClick (t3.text == reponse, decks.Length, b3));

        PanelPageMaisonText.text = deck.info.response;
        b1.gameObject.SetActive (true);
        b2.gameObject.SetActive (true);
        b3.gameObject.SetActive (true);

        // update chances
        float chances = getChancesWith (target);
        ChanceText.text = Data.getChanceLabel (chances);
    }

    private static float getChancesWith (House target) {
        int targetRent = target.getRent ();
        int ourRent = GameState.current.playerHouse.getRent () + GameState.current.getTargetProgress ().approachPoints;
        float chances = Data.config.getSuccessChance (ourRent - targetRent);
        return chances;
    }

    private void TryChoper (float chances) {
        Debug.Log ("Chances de réussite : " + chances);
        double rand = RandomHelper.Random01 ();
        Debug.Log ("Jet de dés à dépasser : " + rand);
        if (chances > rand) {
            TriggerBuildNewHome ();
        } else {
            PlayFailChoperFeedback ();
        }
    }

    private void TriggerBuildNewHome () {
        Debug.Log ("Succes ! Building a new Home");
        Debug.Log ("Starting reproduction process…");
        List<string> changelog = GameState.current.playerHouse.UpgradeWith (
            Data.GetHouse (GameState.current.targetHouseId)
        );
        GameState.current.generation++;
        GameState.current.getTargetProgress ().alreadyDated = true;
        PanelPageUpgrade.SetActive (true);
        ModelDisplayScript.model.SetActive (false);
        this.StartCoroutine (AnimateCopulation (changelog, 50));
    }

    private IEnumerator AnimateCopulation (List<string> changelog, int iterationCount) {
        bool display1 = true;
        var img1 = PanelPageUpgrade.transform.GetChild (0);
        var img2 = PanelPageUpgrade.transform.GetChild (1);
        for (int i = 0; i < iterationCount; i++) {
            img1.gameObject.SetActive (display1);
            img2.gameObject.SetActive (!display1);
            display1 = !display1;

            if (i / 2 < changelog.Count && i % 2 == 1) {
                float x = (float) RandomHelper.Random01 () * -550 + 100;
                float y = (float) RandomHelper.Random01 () * 500 - 300;
                FeedbackBubble f = Instantiate (FeedbackPrefab, PanelPageUpgrade.transform);
                f.duration = 2000;
                f.text = changelog[(i - 1) / 2];
                f.width = 400;
                f.x = x;
                f.y = y;
            }

            yield return new WaitForSeconds (TweenCore.Easing.CubicIn (1 - ((float) i / (float) iterationCount)));
        }
    }

    private void PlayFailChoperFeedback () {
        PanelPageChoper.SetActive (false);
        PanelPageMaison.SetActive (true);
        GameState.current.getTargetProgress ().patience = 0;
        PanelPageMaisonText.text = "Fais plus attention à moi si tu veux avoir tes chances.";
    }
    private void PlayWrongAnswerFeedback (Button b) {
        Debug.Log ("Wrong answer !");
        var f = Instantiate (FeedbackPrefab, b.transform);
        f.text = "X";
        f.duration = 1;
        f.width = 100;
        f.dialog.fontSize = 40;
        f.dialog.color = new Color (1, 0.2f, 0.2f);
        // TODO visual feedback for player
    }

    private void PlayRightAnswerFeedback (Button b) {
        Debug.Log ("Right answer.");
        var f = Instantiate (FeedbackPrefab, b.transform);
        f.text = "✓";
        f.duration = 1;
        f.width = 100;
        f.dialog.fontSize = 40;
        f.dialog.color = new Color (0.2f, 1, 0.2f);
        // TODO visual feedback for player
        // animate chance value
    }
}