﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PanelEnding : MonoBehaviour {

    public Image image1;
    public Image image2;
    public Image image3;
    public Image image4;
    public Image image5;
    public Image image6;
    public Text text;

    public Vector3 targetPosition = new Vector3(100, 0, 0);
    public float speed = 10.0f;
    public float threshold = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update () {

        Vector3 direction = targetPosition - image1.transform.position;
        if (direction.magnitude > threshold)
        {
            direction.Normalize();
            image1.transform.position = image1.transform.position + direction * speed * Time.deltaTime;
        }

    }
}