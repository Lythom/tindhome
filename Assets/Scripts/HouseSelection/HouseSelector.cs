﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HouseSelector : MonoBehaviour {

    public Transform housesButtonsParent;
    public FeedbackBubble Feedback;

    // Start is called before the first frame update
    void Start () {

        for (int i = 0; i < Data.houses.Length; i++) {
            House house = Data.houses[i];
            var houseButton = housesButtonsParent.Find ("Button" + house.name).GetComponentInChildren<Button> ();
            houseButton.onClick.AddListener (StartDateHandler (house, i, houseButton));
            if (GameState.current.getProgress (i).alreadyDated) {
                houseButton.GetComponentInChildren<Outline> ().effectColor = new Color (0.5f, 0.5f, 0.5f);
            }
        }
    }
    UnityAction StartDateHandler (House house, int index, Button b) {
        return () => {
            if (GameState.current.getProgress (index).alreadyDated) {
                var f = Instantiate (Feedback, b.transform);
                f.text = "On t'aime bébé.";
            } else {
                if (AcceptDate (house, GameState.current)) {
                    GameState.current.targetHouseId = index;
                    SceneManager.LoadScene ("HouseDating");
                } else {
                    var f2 = Instantiate (Feedback, b.transform);
                    f2.text = "Non mais\ntu t'es vu ?";
                }
            }
        };
    }

    private bool AcceptDate (House h, GameState state) {
        float targetRent = h.getRent ();
        Debug.Log ("Son loyer " + targetRent);
        float playerRent = state.playerHouse.getRent ();
        Debug.Log ("Mon loyer " + playerRent);
        return Data.config.getSuccessChance (playerRent - targetRent) > 0;
    }

    // Update is called once per frame
    void Update () {

    }
}