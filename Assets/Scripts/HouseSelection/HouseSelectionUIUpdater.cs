﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HouseSelectionUIUpdater : MonoBehaviour {
    public GameObject roomsUnlockedParent;
    public Text rentText;
    public Text generation;
    public Text houseArchetype;
    RoomUnlocked[] roomsUnlocked;

    // Start is called before the first frame update
    void Start () {
        roomsUnlocked = roomsUnlockedParent.GetComponentsInChildren<RoomUnlocked> ();
        rentText.text = GameState.current.playerHouse.getRent () + " €";
        generation.text = GameState.current.generation.ToString ();
        houseArchetype.text = StringExtensions.FirstCharToUpper(Data.getArchetypeLabel (GameState.current.playerHouse.archetype));
        foreach (RoomUnlocked ru in roomsUnlocked) {
            Room playerRoom = GameState.current.playerHouse.rooms.Where (r => r.type == ru.room).FirstOrDefault ();
            ru.isUnlocked = playerRoom != null;
            if (playerRoom != null) {
                ru.level = playerRoom.level;
            }
        }
    }

    // Update is called once per frame
    void Update () {

    }
}