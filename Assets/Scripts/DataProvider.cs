﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class DataProvider : MonoBehaviour {
    void Awake () {
#if UNITY_EDITOR
        var assetPath = Application.streamingAssetsPath;
        var watcher = new FileSystemWatcher (assetPath, "*.json");
        watcher.EnableRaisingEvents = true;
        var handler = new System.IO.FileSystemEventHandler (OnChanged);
        watcher.Changed += handler;
#endif
        LoadData ();
        if (GameState.current == null) {
            GameState.current = new GameState ();
        }
    }

    private void OnChanged (object s, FileSystemEventArgs e) {
        LoadData ();
    }

    public void LoadData () {
        // config
        Data.config = JsonUtility.FromJson<Config> (File.ReadAllText (Application.streamingAssetsPath + "/config.json"));

        // houses
        var houseText = File.ReadAllText (Application.streamingAssetsPath + "/houses.json");
        Data.houses = JsonHelper.getJsonArray<House> (houseText);

        Debug.Log ("Loaded " + Data.houses.Length + " houses");

        // decks
        // Data.collection = new Dictionary<string, QuestionDeck> ();
        // var deckFolderPath = Application.streamingAssetsPath + "/decks";
        // foreach (string file in System.IO.Directory.GetFiles (deckFolderPath, "*.json")) {
        //     var infoValueKey = file.Replace (deckFolderPath + "\\", "").Replace (".json", "");
        //     var fileText = File.ReadAllText (file);
        //     QuestionDeck deck = JsonUtility.FromJson<QuestionDeck> (fileText);
        //     Data.collection.Add (infoValueKey, deck);
        // }

        // Debug.Log("Loaded " + Data.collection.Keys.Count + " decks");
    }

    // Update is called once per frame
    void Update () {

    }
}