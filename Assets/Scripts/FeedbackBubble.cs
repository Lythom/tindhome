﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackBubble : MonoBehaviour {
    public string text = "Non mais\ntu t'es vu !";
    public Text dialog;
    public float duration = 2;
    public float width = 150;
    public Image image;
    public float x;
    public float y;

    RectTransform t;
    float animDuration = 1;
    float startTime = 0;
    float startX;

    // Start is called before the first frame update
    void Awake () {
        t = (RectTransform) this.transform;
        image = this.GetComponent<Image> ();
        startTime = Time.time;
        dialog = GetComponentInChildren<Text> ();
    }

    // Update is called once per frame
    void Update () {
        float animProgress = Mathf.Clamp01 ((Time.time - startTime) / animDuration);
        if (t != null) {
            t.localPosition = new Vector3 (x, y + TweenCore.Easing.ElasticOut (animProgress) * 80, -30);
            t.sizeDelta = new Vector2 (width, t.sizeDelta.y);
        }
        if (startTime + duration < Time.time) {
            Destroy (this.gameObject);
        }
        dialog.text = text;
    }
}