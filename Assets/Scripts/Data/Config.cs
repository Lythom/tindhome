using System;
using UnityEngine;

[System.Serializable]
public struct ConditionPoints {
    public int[] points;
}
public class Config {
    /* Array of RoomType per level points  */
    public ConditionPoints[] roomTypeLevelPoints;

    /* Array of archetypes rent multiplicator */
    public int[] archetypeMultiplicator;

    /* Array of condition rent multiplicator */
    public float[] conditionMultiplicator;

    public int maxJauge = 3;
    public int maxDifferenceForRefused = 1000;

    public int patienceCostRepeat = 1;
    public int patienceQuestionCost = 1;
    public int patienceFailApproach = 3;
    public int rightAnswerApproachBonus = 100;

    /**
        @param scoreDifference positive if player is above, negative if player is below
     */
    public float getSuccessChance (float scoreDifference) {
        if (scoreDifference > 0) return 1;
        float diff = Mathf.Clamp (-scoreDifference, 0, this.maxDifferenceForRefused);
        float ratio = diff / this.maxDifferenceForRefused;
        float result = 1 - TweenCore.Easing.QuadInOut (ratio);
        return result;
    }
}