using System.Collections.Generic;

[System.Serializable]
public class QuestionDeck {
    public QuestionReponse info;
    public QuestionChallenge challenge;
}