/* Available at: https://gist.github.com/mniak/a4d09264ad1ca40c489178325b98935b */
using System;
using System.Linq;
using System.Runtime.Serialization;

public static class EnumHelper {
    public static string Serialize<TEnum> (TEnum value) {
        var fallback = Enum.GetName (typeof (TEnum), value);
        var member = typeof (TEnum).GetMember (value.ToString ()).FirstOrDefault ();
        if (member == null)
            return fallback;
        var enumMemberAttributes = member.GetCustomAttributes (typeof (EnumMemberAttribute), false).Cast<EnumMemberAttribute> ().FirstOrDefault ();
        if (enumMemberAttributes == null)
            return fallback;
        return enumMemberAttributes.Value;
    }
    public static TEnum Deserialize<TEnum> (string value) where TEnum : struct {
        TEnum parsed;
        try {
        parsed = (TEnum) Enum.Parse (typeof (TEnum), value);
        return parsed;
        } catch (Exception) { }

        var found = typeof (TEnum).GetMembers ()
            .Select (x => new {
                Member = x,
                    Attribute = x.GetCustomAttributes (typeof (EnumMemberAttribute), false).OfType<EnumMemberAttribute> ().FirstOrDefault ()
            })
            .FirstOrDefault (x => x.Attribute == null ? false : x.Attribute.Value == value);
        if (found != null)
            return (TEnum) Enum.Parse (typeof (TEnum), found.Member.Name);
        return default (TEnum);
    }
}