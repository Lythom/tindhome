﻿using System;
using UnityEngine;

public class JsonHelper
 {
     //Usage:
     //YouObject[] objects = JsonHelper.getJsonArray<YouObject> (jsonString);
     public static T[] getJsonArray<T>(string json)
     {
         string newJson = json;
         try {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
            return wrapper.data;
         } catch (Exception e) {
             Debug.LogError(e);
         }
         return null;
     }
 
     //Usage:
     //string jsonString = JsonHelper.arrayToJson<YouObject>(objects);
     public static string arrayToJson<T>(T[] array, bool prettyPrint = true)
     {
         Wrapper<T> wrapper = new Wrapper<T>();
         wrapper.data = array;
         return JsonUtility.ToJson(wrapper, prettyPrint);
     }
 
     [System.Serializable]
     private class Wrapper<T>
     {
         public T[] data;
     }
 }