﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public enum RoomType {
    bedroom,
    kitchen,
    bathroom,
    bedroom2,
    livingroom,
    garage,
    swimmingpool,
    cave
}

public enum RoomLevel {
    cheap,
    cocooning,
    design
}

[System.Serializable]
public class Room {
    public RoomType type;
    public RoomLevel level;

    public int getScore () {
        var val = Data.config.roomTypeLevelPoints[(int) type].points[(int) level];
        Debug.Log ("Room points ( " + type.ToString () + "," + level.ToString () + ")" + val);
        return val;
    }
}