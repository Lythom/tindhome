[System.Serializable]
public class QuestionChallenge {
    public string question;
    public string[] wrongResponses;
    public string rightResponse;
}