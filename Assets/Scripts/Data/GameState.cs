﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Progress {
    public int approachPoints;
    public int patience;
    public List<string> alreadyAsked = new List<string> ();
    public bool alreadyDated = false;
}

public class GameState {
    public static GameState current = new GameState ();

    public House playerHouse = new House ();
    public int targetHouseId = 0;
    private Dictionary<int, Progress> progress = new Dictionary<int, Progress> ();
    public int generation = 1;

    public void PassTurn (Progress except) {
        foreach (Progress p in progress.Values) {
            if (p != except) {
                p.patience++;
            }
            p.patience = Mathf.Clamp (p.patience, 0, Data.config.maxJauge);
        }
    }

    public Progress getTargetProgress () {
        if (Data.GetHouse (targetHouseId) == null) return null;
        return getProgress (targetHouseId);
    }
    public Progress getProgress (int houseId) {
        if (!progress.ContainsKey (houseId)) {
            progress[houseId] = new Progress () {
                approachPoints = 0,
                patience = 3
            };
        }
        return progress[houseId];
    }
}