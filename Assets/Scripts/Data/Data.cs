﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Data {
    public static House[] houses = new House[0];
    public static Config config = new Config ();
    public static Dictionary<string, QuestionDeck> collection = new Dictionary<string, QuestionDeck> ();

    public static House GetHouse (int houseId) {
        if (houseId < 0 || houseId >= houses.Length) return null;
        return houses[houseId];
    }

    // INFORMATIONS
    // X infos à découvrir parmis  {
    // nombre de pièce
    // niveau maison
    // nom
    // description
    // type_pièce
    // niveau_pièce
    // archetype_piece
    // état_pièce
    // }
    // une info/valeur = List<Question -> Réponse>

    // APPROCHE
    // QUESTION / REPONSE / REPONSE / REPONSE (une seule vraie) par info/valeur

    public static string getArchetypeLabel (Archetype a) {
        switch (a) {
            case Archetype.MobileHome:
                return "caravane";
            case Archetype.StudioT1:
                return "studio T1";
            case Archetype.StudioT2:
                return "studio T2";
            case Archetype.StudioT3:
                return "studio T3";
            case Archetype.Loft:
                return "Loft";
            case Archetype.ArchitectHouse:
                return "Maison d'architecte";
            default:
                return "unknown archetype";
        }
    }

    public static string[] houseLevelLabel = new string[] { "mobile-home", "T1", "T2" };
    public static string getHouseLevelLabel (int level) {
        int l = Mathf.Clamp (level, 0, houseLevelLabel.Length);
        return houseLevelLabel[l];
    }
    public static string getRoomTypeLabel (RoomType rt) {
        switch (rt) {
            case RoomType.bedroom:
                return "chambre";
            case RoomType.kitchen:
                return "cuisine";
            case RoomType.bathroom:
                return "salle de bain";
            case RoomType.bedroom2:
                return "chambre d'ami";
            case RoomType.livingroom:
                return "salon";
            case RoomType.garage:
                return "garage";
            case RoomType.swimmingpool:
                return "piscine";
            case RoomType.cave:
                return "cave";
            default:
                return "unknown room type";
        }
    }

    public static string getConditionLabel (Condition c) {
        switch (c) {
            case Condition.dilapidated:
                return "Insalubre";
            case Condition.good:
                return "normal";
            case Condition.newBuild:
                return "rénové";
            default:
                return "unknown condition";
        }
    }

    public static string getRoomLevelLabel (RoomLevel level) {
        var list = new string[] { "bon marché", "cocooning", "design" };
        int l = Mathf.Clamp ((int) level, 0, list.Length);
        return list[l];
    }

    internal static string getChanceLabel (float chances) {
        string labelChance = "Chances de succès : ";
        if (chances < 0.20) {
            labelChance += "Va falloir assurer";
        } else if (chances < 0.40) {
            labelChance += "Sur un malentendu…";
        } else if (chances < 0.60) {
            labelChance += "Je lui plait un peu";
        } else if (chances < 0.80) {
            labelChance += "Ça peut marcher";
        } else {
            labelChance += "Mon charme opère";
        }
        return labelChance;
    }
}