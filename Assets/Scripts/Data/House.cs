﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Archetype {
    MobileHome,
    StudioT1,
    StudioT2,
    StudioT3,
    Loft,
    ArchitectHouse
}

public enum Condition {
    dilapidated,
    good,
    newBuild
}

[System.Serializable]
public class House {

    public House () {
        this.name = "Home sweet Home";
        this.description = "C'est chez nous !";
        this.rooms = new Room[] {
            new Room (),
            new Room ()
        };
        this.rooms[0].type = RoomType.bathroom;
        this.rooms[0].level = RoomLevel.cheap;
        this.rooms[1].type = RoomType.bedroom;
        this.rooms[1].level = RoomLevel.cheap;
        this.archetype = Archetype.MobileHome;
        this.condition = Condition.good;
    }

    public string name;
    public string description;
    public Room[] rooms = new Room[0];
    public Archetype archetype;
    public Condition condition;
    public QuestionDeck[] decks;

    public int getRent () {
        int roomScore = rooms.Sum (room => room.getScore ());
        int archetypeMult = Data.config.archetypeMultiplicator[(int) archetype];
        float conditionMult = Data.config.conditionMultiplicator[(int) condition];
        return (int) Math.Floor (roomScore * archetypeMult * conditionMult); // formula to calculate the rent from house properties
    }

    public List<string> UpgradeWith (House mate) {
        var newRooms = new List<Room> ();
        var changelog = new List<String> ();
        foreach (Room mateRoom in mate.rooms) {
            var myRoomsOfSameType = this.rooms.Where (myRoom => myRoom.type == mateRoom.type);
            var sameRoomCount = myRoomsOfSameType.Count ();
            if (sameRoomCount == 0) {
                // if I don't have the room I acquire it at minimum level
                var r = AddRoom (newRooms, mateRoom);
                changelog.Add ("Nouveau ! Votre maison obtient un·e " + Data.getRoomTypeLabel (r.type) + " " + Data.getRoomLevelLabel (r.level));
            } else if (sameRoomCount > 0) {
                foreach (var myRoom in myRoomsOfSameType) {
                    // Any room same or better is upgraded
                    // Any room worst in downgraded
                    if (myRoom.level <= mateRoom.level) {
                        Upgrade (myRoom, mateRoom);
                        changelog.Add ("Amélioration ! " + Data.getRoomTypeLabel (myRoom.type) + " est maintenant " + Data.getRoomLevelLabel (myRoom.level));
                    } 
                    // else {
                    //     Downgrade (myRoom, mateRoom);
                    //     changelog.Add ("Oh non ! " + Data.getRoomTypeLabel (myRoom.type) + " redevient " + Data.getRoomLevelLabel (myRoom.level));
                    // }
                }
            }
        }

        this.rooms = newRooms.Concat (this.rooms).ToArray ();
        this.archetype = getArchetypeFromRooms ();
        return changelog;
    }

    private Archetype getArchetypeFromRooms () {
        // all rooms = architect
        if (this.rooms.Count () == 8) {
            return Archetype.ArchitectHouse;
        }
        // all rooms but swimming ppool = loft
        if (this.rooms.Count (r => r.type == RoomType.swimmingpool) == 0 && this.rooms.Count () == 7) {
            return Archetype.Loft;
        }
        // all rooms but swimming ppool and cave = T3
        if (this.rooms.Count (r => r.type == RoomType.cave) == 0 && this.rooms.Count (r => r.type == RoomType.swimmingpool) == 0 && this.rooms.Count () == 6) {
            return Archetype.StudioT3;
        }
        // specific T2 rooms
        if (this.rooms.Count (r => r.type == RoomType.bathroom) == 1 &&
            this.rooms.Count (r => r.type == RoomType.kitchen) == 1 &&
            this.rooms.Count (r => r.type == RoomType.bedroom) == 1 &&
            this.rooms.Count (r => r.type == RoomType.livingroom) == 1
        ) {
            return Archetype.StudioT2;
        }
        // specific T1 rooms
        if (this.rooms.Count (r => r.type == RoomType.bathroom) == 1 &&
            this.rooms.Count (r => r.type == RoomType.bedroom) == 1 &&
            this.rooms.Count (r => r.type == RoomType.livingroom) == 1
        ) {
            return Archetype.StudioT1;
        }
        return Archetype.MobileHome;
    }

    private static Room AddRoom (List<Room> newRooms, Room mateRoom) {
        Room r = new Room ();
        r.type = mateRoom.type;
        r.level = RoomLevel.cheap;
        newRooms.Add (r);
        Debug.Log ("Nouvelle " +
            Data.getRoomTypeLabel (r.type) +
            " " +
            Data.getRoomLevelLabel (r.level)
        );
        return r;
    }

    private static Room Upgrade (Room myRoom, Room mateRoom) {
        switch (myRoom.level) {
            case RoomLevel.cheap:
                myRoom.level = RoomLevel.cocooning;
                break;
            case RoomLevel.cocooning:
                myRoom.level = RoomLevel.design;
                break;
        }
        Debug.Log ("Amélioration de " +
            Data.getRoomTypeLabel (myRoom.type) +
            " en " +
            Data.getRoomLevelLabel (myRoom.level)
        );
        return myRoom;
    }

    private static Room Downgrade (Room myRoom, Room mateRoom) {
        // downgrade if other is worst
        switch (myRoom.level) {
            case RoomLevel.design:
                myRoom.level = RoomLevel.cocooning;
                break;
            case RoomLevel.cocooning:
                myRoom.level = RoomLevel.cheap;
                break;
        }
        Debug.Log ("Rétrogradation de " +
            Data.getRoomTypeLabel (myRoom.type) +
            " en " +
            Data.getRoomLevelLabel (myRoom.level)
        );
        return myRoom;
    }
}