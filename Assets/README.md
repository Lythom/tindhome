# Les données de TindHome

Les valeurs de ce document ne peuvent être modifiées que dans le code. Demander à un Dave si besoin.

## Emplacement des fichiers de données

La liste des maison est éditable dans `StreamingAssets/houses.json`




## Signification des valeurs

### House level
0 = mobile-home
1 = T1
2 = T2

### House archetype
0 = caravane
1 = studio T1
2 = studio T2
3 = studio T3
4 = Loft
5 = Maison d'architecte

### Room conditions
0 = Insalubre
1 = normal
2 = rénové

### Room types
0 = chambre
1 = cuisine
2 = salle de bain
3 = chambre d'ami
4 = salon
5 = garage
6 = piscine
7 = cave

### Room level
0 = bon marché
1 = cocooning
2 = design
