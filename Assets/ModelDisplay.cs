﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelDisplay : MonoBehaviour {
    public GameObject models;
    public GameObject model;
    private string showing = "";

    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {
        House target = Data.GetHouse (GameState.current.targetHouseId);
        if (target == null) return;
        string candidate = target.name;
        if (candidate != showing) {
            model = Instantiate(
                models.transform.Find(candidate).gameObject,
                new Vector3(-0.4f, -0.4f, -0.4f),
                Quaternion.identity
            );
            model.SetActive(true);
            model.transform.localScale = Vector3.one;
            showing = candidate;

        }

        model.transform.Rotate(new Vector3(0, 3 * Time.deltaTime, 0));
    }
}